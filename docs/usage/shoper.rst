Shoper
======



Instalacja
^^^^^^^^^^



#. Zaloguj się do panelu admina swojego sklepu
#. Wejdź do *Dodatki i integracje* > *Aplikacje*
#. Wyszukaj *Shopnavigator*: kliknij *WYBIERZ FILTRY* > *Szukaj* wpisz *Shopnavigator* i kliknij *FILTRUJ*
#. Kliknij zainstaluj, wyraź zgodę na uprawnienia (są niezbędne do działania Shopnavigatora)
#. Jeżeli nie posiadasz konta `<http://app.shopnavigator.net/>`_ to utwórz je, klikając *Sign Up* i następnie podając nazwę użytkownika, swój adres email oraz hasło
#. Zaloguj się na swoje konto w `<http://app.shopnavigator.net/>`_
#. W panelu admina Shoper w *Moje Aplikacje* otwórz stronę modułu ShopNavigator i podaj nazwę użytkownika i hasło używane w `<http://app.shopnavigator.net/>`_ 
#. Otrzymasz informację o tym czy działanie się powiodło, jeżeli nie spróbuj połączyć używając `<http://app.shopnavigator.net/>`_
#. Kliknij w *Link your account to Shoper shop* znajdujący się w sekcji *Integration Status*
#. Wpisz pełny link (zaczynający się od https) do strony głównej swojego sklepu i kliknij *Apply*



Konfiguracja
^^^^^^^^^^^^



Ustawieniami Shopnavigator steruje się poprzez `<http://app.shopnavigator.net/>`_.
Panel ustawień zostanie otwarty po kliknięciu w *Settings* w sekcji *Integration Status*, brak *Settings* oznacza, że instalacja nie została ukończona.
Dostępne ustawienia zebrano w tabeli. Po kliknięciu w *Apply* dokonana zostanie próba ich wprowadzenia, a informacja o jej wyniku wyświetlona. Uwaga: zmiana zajmuje trochę czasu.

+------------------------+------------------+-------------------------------------------------------------------------------------------+
| Ustawienie             | Możliwe wartości | Opis                                                                                      |
+------------------------+------------------+-------------------------------------------------------------------------------------------+
| Recommendations        | On/Off           | Włącza/wyłącza wyświetlanie dodatkowych (wbudowane pozostają bez zmian) poleceń produktów |
+------------------------+------------------+-------------------------------------------------------------------------------------------+
| Search                 | On/Off           | Włącza/wyłącza wyszukiwanie zastępujące wbudowane w Shoper wyszukiwanie                   |
+------------------------+------------------+-------------------------------------------------------------------------------------------+
| Search highlight color | Kolor CSS        | Ustawia kolor podświetlenia wybranego produktu w wyszukiwaniu                             |
+------------------------+------------------+-------------------------------------------------------------------------------------------+
