Prestashop
==========



Moduły ShopNavigator do Prestashop przeznaczone są dla wersji 1.7 lub wyższej Prestashop. Moduł *ShopNavigator search* zapewnia wyszukiwanie, natomiast *ShopNavigator recommendations* polecenia produktów.



Instalacja - search
^^^^^^^^^^^^^^^^^^^



#. Jeżeli nie posiadasz konta `<http://app.shopnavigator.net/>`_ to utwórz je, klikając *Sign Up* i następnie podając nazwę użytkownika, swój adres email oraz hasło
#. Zaloguj się na swoje konto w `<http://app.shopnavigator.net/>`_
#. Wyświetl token klikając na *view* obok *Your API token* i zapisz go 
#. Pobierz *ShopNavigator search* `<http://app.shopnavigator.net/static/presta_shopnavigator_search.zip>`_
#. Zaloguj się do panelu admina swojego sklepu
#. Wejdź w *ULEPSZENIA* > *Moduły* > *Module Manager*
#. Wyłącz wbudowane wyszukiwanie: wyszukaj *Pasek Szukania* i wyłącz go (zob. zrzut)
#. Kliknij w *Załaduj Moduł* i wybierz plik ściągnięty wcześniej
#. Po zakończeniu instalacji modułu, konieczne jest jego prawidłowe skonfigurowanie (zob. **Ustawienia konfiguracyjne**), inaczej nie będzie on działał

.. image:: /images/searchbaroff.png

Wyłączanie wbudowanego paska szukania



Instalacja - recommendations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^



#. Jeżeli nie posiadasz konta `<http://app.shopnavigator.net/>`_ to utwórz je, klikając *Sign Up* i następnie podając nazwę użytkownika, swój adres email oraz hasło
#. Zaloguj się na swoje konto w `<http://app.shopnavigator.net/>`_
#. Wyświetl token klikając na *view* obok *Your API token* i zapisz go 
#. Pobierz *ShopNavigator recommendations* `<http://app.shopnavigator.net/static/presta_shopnavigator_reco.zip>`_
#. Zaloguj się do panelu admina swojego sklepu
#. Wejdź w *ULEPSZENIA* > *Moduły* > *Module Manager*
#. Kliknij w *Załaduj Moduł* i wybierz plik ściągnięty wcześniej (pierwszy z nich, jeżeli pobrano oba)
#. Po zakończeniu instalacji modułu, konieczne jest jego prawidłowe skonfigurowanie (zob. **Ustawienia konfiguracyjne**), inaczej nie będzie on działał



Ustawienia konfiguracyjne
^^^^^^^^^^^^^^^^^^^^^^^^^



Ustawienia konfiguracyjne wyglądają tak samo dla obu modułów

+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Nazwa                                                                      | Przeznaczenie                                                   | Uwagi                                                                                                                               |
+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Shopnavigator username                                                     | Nazwa użytkownika                                               | Nazwa użytkownika w `<http://app.shopnavigator.net/>`_                                                                              |
+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| API KEY                                                                    | Token użytkownika                                               | Dostępny po zalogowaniu do `<http://app.shopnavigator.net/>`_ jako *Your API token*                                                 |
+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Your shop hash                                                             | Identyfikator twojego sklepu wykorzystywany przez Shopnavigator | Pozostawienie pustego spowoduje wygenerowanie na podstawie nazwy użytkownika, jeżeli podajesz własny musi mieć co najmniej 6 znaków |
+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Existing WebserviceKey with GET and HEAD access to products and categories | Klucz API do pobierania danych o produktach i kategoriach       | Generowany automatycznie przy instalacji modułu                                                                                     |
+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+
| Shopnavigator url                                                          | Adres serwera ShopNavigator                                     | Adres serwera ShopNavigator na którym posiadasz swoje konto                                                                         |
+----------------------------------------------------------------------------+-----------------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------+

Wszystkie pola poza *Your shop hash* są wymagane (nie mogą pozostać puste). Możliwe efekty po kliknięciu w *Zapisz* to:

#. Settings updated - wszystko zadziałało, jak należy
#. Invalid Configuration value - którejś wartości brakuje lub jest błędna
#. Unable to add shop_hash to shopnavigator database - nie udało się dodać sklepu o danym shop_hash, sprawdź czy został podany właściwy API KEY (token użytkownika), jeżeli to nie da efektu możliwe, że ktoś już wykorzystuje podany shop hash, spróbuj zmienić *Your shop hash* na inny
