Inne
====



Instalacja - search
^^^^^^^^^^^^^^^^^^^



Uwaga: do instalacji tego mechanizmu wymagana jest znajomość podstaw języka *JavaScript* i zapytań *HTTP*.

#. Jeżeli nie posiadasz konta `<http://app.shopnavigator.net/>`_ to utwórz je, klikając *Sign Up* i następnie podając nazwę użytkownika, swój adres email oraz hasło
#. Zaloguj się na swoje konto w `<http://app.shopnavigator.net/>`_
#. Odnajdź *Link your shop* w *Integration status* z rozwijanej listy wybierz *Other (generic)*
#. Podaj url, z którego można pobrać plik XML opisujący produkty w twoim sklepie, obecnie żaden mechanizm autoryzacji nie jest wspieranie to jest plik musi być ogólnie dostępny
#. Wciśnij *Save*. Zostanie przydzielony identyfikator (ShopNavigator's identifier), zapisz go
#. Jeżeli podany plik jest w jednym ze wspieranych standartów informacje o produktach zostaną pobrane. Czas na to potrzebny zależy od liczby produktów i może to być kilka minut
#. Odczekaj kilka minut, wyślij zapytanie ``JSON`` ``POST`` na adres ``https://app.shopnavigator.net/v1/search/IDENTYFIKATOR`` o treści ``{}`` gdzie ``IDENTYFIKATOR`` to identyfikator, który został wcześniej przydzielony
#. Sprawdź ``results`` w treści odpowiedzi, jeżeli jest to ``[]`` to plik XML nie jest w jednym ze wspieranych standardów lub dane nie zostały jeszcze przetworzone, jeżeli pojawiły się tam dane o produktach z twojego sklepu możesz kontynuować
#. Zapytania wysyłane do ``https://app.shopnavigator.net/v1/search/IDENTYFIKATOR`` powinny być ``JSON`` ``POST`` i zawierać szukaną frazę pod ``q`` przykładowo ``{"q":"bluza"}``
#. W odpowiedzi na zapytanie ShopNavigator przysyła ``JSON`` z danymi pasujących kategorii (``categories``) i produktów (``results``), ich przetwarzanie i dalsze wykorzystanie odbywa się po stronie sklepu

Przykładowa odpowiedź na ``{"q":"Shirt"}`` dla sklepu który korzysta z identyfikatora ``0B3A02D7``

.. code-block:: JSON

  {"categories":[{"category_name":" Clothing > Dresses ","category_url":null}],"metadata":"metadata","results":[{"category_id":"Apparel & Accessories > Clothing > Dresses","category_name":" Clothing > Dresses ","category_path":null,"description":"Solid red, queen-sized bed sheets made from 100% woven polyester 300 thread count fabric. Set includes one fitted sheet, one flat sheet, and two standard pillowcases. Machine washable; extra deep fitted pockets.","description_short":null,"group":" Clothing > Dresses ","id":9244,"imgurl":"http://www.example.com/image1.jpg","name":"Mens Pique Polo Shirt","pid":"tddy123uk","price":1900,"price_sale":1500,"shop_hash":"0B3A02D7","shop_id":8,"url":"http://www.example.com/asp/sp.asp?cat=12&id=1030","url_category":null,"url_img":"http://www.example.com/image1.jpg"}]}

Dostępność danych zależy od wykorzystywanego standartu w pliku XML, brak informacji oznaczany jest poprzez ``null``.

Przykładowa implementacja wykorzystująca `<https://jquery.com/>`_ oraz `<https://github.com/kraaden/autocomplete>`_, ``test`` to specjalny identyfikator, którego użycie powoduje szukanie wśród trzech stałych produktów: ``Mysz``, ``Komputer``, ``Monitor`` 

Zależności

.. code-block:: HTML

  <link rel="stylesheet" href="autocomplete.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="autocomplete.min.js"></script>

Panel search

.. code-block:: HTML

  <div id="shopnavigator_search_widget" data-search-mode="simple" data-search-url="https://app.shopnavigator.net/v1/search/test"><input id="shopnavigator_search_widget_textinput" type="text"></div>

Skrypt

.. code-block:: JavaScript

	$(document).ready(function () {
		var search_mode = document.getElementById("shopnavigator_search_widget").dataset.searchMode;

		var search_url = document.getElementById("shopnavigator_search_widget").dataset.searchUrl;

		const settings = {"searchurl": search_url, "cooldown": 300};
		
		function getSuggestionsSimple(text, update){
			let xhr = new XMLHttpRequest();
			xhr.open("POST", settings["searchurl"], true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onreadystatechange = function () {
				if (xhr.readyState === 4 && xhr.status === 200) {
					var searchanswer = JSON.parse(xhr.responseText);
					update(searchanswer["results"]);
				};
			};
			var data = JSON.stringify({"q":text});
			xhr.send(data);
		}

		function getSuggestionsExtended(text){
			if(!text){
				removeSearchPanel();
				return;
			}
			let xhr = new XMLHttpRequest();
			xhr.open("POST", settings["searchurl"], true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onreadystatechange = function () {
				if (xhr.readyState === 4 && xhr.status === 200) {
					var searchanswer = JSON.parse(xhr.responseText);
					showSuggestions(searchanswer, text);
				};
			};
			var data = JSON.stringify({"q":text});
			xhr.send(data);
		}

		function charsAllowed(value){
			return /^[a-zA-Z\s]+$/.test(value);
		}

		function removeSearchPanel(){
			if(!(document.getElementById("shopnavigator_search_widget_panel")===null)){
				document.getElementById("shopnavigator_search_widget_panel").remove();
			}
		}

		function showSuggestions(items, value){
			var prods_header = document.createElement("div");
			prods_header.innerText = "Products";
			var prods_div = document.createElement("div");
			prods_div.appendChild(prods_header);
			for(item of items["results"]){
				var itemElement = document.createElement("a");
				itemElement.innerHTML = "<img src=\"" + item.imgurl + "\" alt=\"" + item.name + "\" style=\"height:3em;vertical-align:middle;\">";
				if (charsAllowed(value)) {
					var regex = new RegExp(value, 'gi');
					var inner = item.name.replace(regex, function(match) { return "<span>" + match + "</span>" });
					itemElement.innerHTML += inner;
				} else {
					itemElement.textContent += item.name;
				}
				itemElement.href = item.url;
				itemElement.className = "shopnavprod";
				prods_div.appendChild(itemElement);
			}
			prods_div.className = "shopnavprod";
			var cats_div = document.createElement("div");
			for(cat of items["categories"]){
				var catElement = document.createElement("a");
				catElement.innerHTML = "<b>" + cat.category_name + "</b>";
				catElement.href = cat.category_url;
				catElement.className = "shopnavcat";
				cats_div.appendChild(catElement);
			}
			removeSearchPanel();
			var holder = document.createElement("div");
			holder.appendChild(cats_div);
			holder.appendChild(prods_div);
			holder.id = "shopnavigator_search_widget_panel";
			holder.className = "shopnavpanel";
			document.getElementById("shopnavigator_search_widget_suggestions").appendChild(holder);
		}

		var searchInput = document.getElementById("shopnavigator_search_widget_textinput");

		if(search_mode == "simple"){
			autocomplete({
				input: searchInput,
				onSelect: function(item){
					/*searchInput.value = item.name;*/
					window.location.href = item.url;
				},
				fetch: getSuggestionsSimple,
				render: function(item, value) {
					var itemElement = document.createElement("div");
					itemElement.innerHTML = "<img src=\"" + item.imgurl + "\" alt=\"" + item.name + "\" style=\"height:3em;vertical-align:middle;\">";
					if (charsAllowed(value)) {
						var regex = new RegExp(value, 'gi');
						var inner = item.name.replace(regex, function(match) { return "<span>" + match + "</span>" });
						itemElement.innerHTML += inner;
					} else {
						itemElement.textContent += item.name;
					}
					return itemElement;
				},
				renderGroup: function(groupName, currentValue) {
					var div = document.createElement("div");
					div.textContent = groupName;
					return div;
				},
				emptyMsg: "nothing found",
				debounceWaitMs: settings["cooldown"]
			})
		}else if(search_mode == "extended"){
			document.addEventListener("click", function(evt) {
				var shopnavigatorSearchPanel = document.getElementById("shopnavigator_search_widget_panel");
				if(!shopnavigatorSearchPanel){
					return;
				}
				var targetElement = evt.target;
				do {
					if (targetElement == shopnavigatorSearchPanel) {
						return;
					}
					targetElement = targetElement.parentNode;
				} while (targetElement);
				shopnavigatorSearchPanel.remove();
			});

			const debounce = (func, delay) => { /* used for preventing spamming API with calls for each change */
			  let inDebounce
			  return function() {
				const context = this
				const args = arguments
				clearTimeout(inDebounce)
				inDebounce = setTimeout(() => func.apply(context, args), delay)
			  }
			}

			searchInput.oninput = debounce(function(){getSuggestionsExtended(this.value)}, settings["cooldown"]);
		}else{
			console.log(search_mode+" is not supported search_mode");
		}
	});



Instalacja - recommendations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^



Przykład: umieszczanie widżetu z 3 kolumnami i 2 wierszami dla produktu z id 3505

#. Jeżeli nie posiadasz konta `<http://app.shopnavigator.net/>`_ to utwórz je, klikając *Sign Up* i następnie podając nazwę użytkownika, swój adres email oraz hasło
#. Zaloguj się na swoje konto w `<http://app.shopnavigator.net/>`_
#. W swojej stronie umieść ``<div class="dd-recobox dd-col3 dd-row2"></div>``
#. Bezpośrednio po zamknięciu tagu ``body`` umieść ``<script type="text/javascript"> window.ddt={baseUrl:'http://app.shopnavigator.net'}; window.onload=function(){ddt.getRecoBoxes('NAZWAKONTA', 3505);} </script> <script type="text/javascript" src="http://app.shopnavigator.net/scripts/sendData.js" async="true">`` gdzie ``NAZWAKONTA`` to nazwa użyta w drugim kroku

Opcjonalnie dla każdego ``div``-a można ustawić dla którego produktu mają pojawiać się rekomendację, poprzez podanie id poprzez klasę ``dd-prod`` przykładowo, jeżeli produkt ma id 3505 to ``<div class="dd-recobox dd-col3 dd-row2 dd-prod-3505"></div>``

Uwagi

- funkcja ``ddt.getRecoBoxes`` może zostać użyta z pojedynczym argumentem - nazwą konta
- jeżeli ``ddt.getRecoBoxes`` używana jest z jednym argumentem to wszystkie ``div``-y ``dd-recobox`` muszą posiadać id produktu podany przez klasę - ``div``-y ``dd-recobox`` nie spełniające tego warunku nie zadziałają
- w przypadku podania id produktu poprzez klasę ``div``-a oraz jako argument funkcji ``ddt.getRecoboxes`` używane jest to pierwsze
