.. Shopnavigator documentation master file, created by
   sphinx-quickstart on Mon Jan 18 10:31:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Shopnavigator's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/introduction
   usage/prestashop
   usage/shoper
   usage/others



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
